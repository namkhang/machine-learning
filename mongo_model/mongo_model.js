const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userAccount = new Schema({
    username : {type : String , require : true} , 
    password :  {type : String , require : true} ,
    fullname  : String, 
    address : String ,
    phone : String ,
    image : String ,
    gender : String ,
    birthday : String
})
const doctorAccount = new Schema({
    username : {type : String , require : true} , 
    password :  {type : String , require : true} ,
    fullname  : String, 
    address : String ,
    phone : String ,
    image : String ,
    gender : String ,
    birthday : String
})

module.exports.userAccount = mongoose.model('useraccounts' ,userAccount )