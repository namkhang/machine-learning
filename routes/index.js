var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt')
var jwt = require('jsonwebtoken')

const db = require('../mongo_model/mongo_model')
const middleware = require('../middleware/middleware')


/* GET home page. */
router.post('/create-user-account', async (req,res) => {
  try{
    let {username , password , fullname , address , phone , birthday , gender} = req.body
    let dataUser = await db.userAccount.findOne({username})
    if (dataUser){
      res.status(200).json({message : 'User da ton tai'})
    } 
    else{
      let hashPass = await bcrypt.hash(password, 10)
      await db.userAccount.create({username , password : hashPass , fullname , address , phone ,birthday , gender})
      res.status(201).json({success : true})
    }
  }
  catch (err){
      res.status(200).json({message : 'Create Fail'})
  } 
});

router.post('/login-user' , async (req,res) =>{
  let {username , password} = req.body
  let dataUser = await db.userAccount.findOne({username})
  if(dataUser){
      let result = await bcrypt.compare(password , dataUser.password)
      let token = jwt.sign({userID : dataUser._id , fullname : dataUser.fullname} , process.env.SCRET_TOKEN)
      if (result){
        res.status(200).json({
          success : true ,
          token
        })
      }
      else{
        res.status(200).json({message : 'Password Invalid'})
      }
  }
  else{
    res.status(200).json({message : 'Username Invalid'})
  }

})

router.get('/get-info' , middleware.authJWT ,  async(req,res)=>{
      res.status(200).json({data : res.locals.dataUser})
})

module.exports = router;
