const jwt = require('jsonwebtoken')


module.exports.authJWT = (req,res,next) =>{
    let token = req.headers.authorization.split(' ')[1]
    if(token){
        try{
            let dataUser = jwt.verify(token , process.env.SCRET_TOKEN)
            res.locals.dataUser = dataUser
            next()
            
        }
        catch (err){
                res.status(200).json({message : 'Token Invalid'})
        }
        
    }
    else{
        res.status(200).json({message : 'No token'})
    }

}